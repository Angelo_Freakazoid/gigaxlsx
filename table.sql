CREATE TABLE certificateur (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  intitule TEXT NOT NULL
);

CREATE TABLE diplome (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  intitule TEXT NOT NULL
);

CREATE TABLE certificat (
  code_rncp TEXT PRIMARY KEY NOT NULL,
  intitule TEXT NOT NULL,
  certificateur_id INTEGER NOT NULL,
  diplome_id INTEGER NOT NULL,
  FOREIGN KEY(certificateur_id) REFERENCES certificateur(id),
  FOREIGN KEY(diplome_id) REFERENCES diplome(id)
);

CREATE TABLE CPNE (
  code_cpne INTEGER PRIMARY KEY NOT NULL,
  intitule TEXT NOT NULL
);

CREATE TABLE IDCC (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  code_idcc INTEGER NOT NULL UNIQUE, 
  cpne_id INTEGER NOT NULL,
  FOREIGN KEY(cpne_id) REFERENCES CPNE(code_cpne)
);

CREATE TABLE application (
  certificat_code_rncp TEXT NOT NULL, 
  code_cpne INTEGER NOT NULL,
  statut TEXT NOT NULL,
  tarif REAL NOT NULL,
  date TEXT NOT NULL,
  PRIMARY KEY (certificat_code_rncp, code_cpne), 
  FOREIGN KEY(certificat_code_rncp) REFERENCES certificat(code_rncp),
  FOREIGN KEY(code_cpne) REFERENCES CPNE(code_cpne)
);
